#!/usr/bin/env bash
sh stop.sh
docker-compose -p django-bookkeeping -f docker-compose-arm64.yml pull
docker-compose -p django-bookkeeping -f docker-compose-arm64.yml up -d
