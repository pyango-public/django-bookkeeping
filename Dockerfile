FROM python:3.9.15-slim

RUN apt-get update && apt-get upgrade -y && apt-get install -qqy \
    curl \
    wget \
    bzip2 \
    graphviz \
    libssl-dev \
    openssh-server \
    libgdk-pixbuf2.0-0 \
    gettext \
    libjpeg-dev \
    libatlas-base-dev \
    build-essential  \
    libffi-dev \
    libpq-dev \
    python-dev

RUN mkdir /var/run/sshd \
  && echo 'root:screencast' | /usr/sbin/chpasswd \
  && sed -i '/PermitRootLogin/c\PermitRootLogin yes' /etc/ssh/sshd_config \
  && sed -i '/AllowTcpForwarding/c\AllowTcpForwarding yes' /etc/ssh/sshd_config \
  # SSH login fix. Otherwise user is kicked off after login
  && sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /usr/sbin/sshd \
  && echo "export VISIBLE=now" >> /etc/profile \
  && ssh-keygen -A
ENV NOTVISIBLE "in users profile"

# Create some folders
RUN mkdir -p /app | \
    mkdir -p /media-files | \
    mkdir -p /static-files

COPY ./app/requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
COPY ./app /app
COPY ./scripts/* /scripts/
RUN chmod +x /scripts/*

WORKDIR /app

EXPOSE 8000
EXPOSE 22