import uuid


def upload_to_s3(filename, args):
    filename_arr = filename.rsplit('.')
    if len(filename_arr) > 1:
        filename = filename.replace('.' + filename_arr[-1], uuid.uuid4().hex) + '.' + filename_arr[-1]
    else:
        filename += filename + uuid.uuid4().hex
    filename = args + '/' + filename
    return filename
