from django.urls import path
from .views import ForwardAuthView

app_name = 'rest_api'

urlpatterns = [
    path('auth/', ForwardAuthView.as_view(), name='auth'),
]
