from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseForbidden
from django.views import View


class ForwardAuthView(LoginRequiredMixin, View):
    """
    This View is here to be extended and have the check_permissions functions overwritten
    """

    def check_permissions(self):
        user = self.request.user
        if not user.is_staff or not user.is_superuser:
            return False
        return True

    def get(self, request, **kwargs):
        if not self.check_permissions():
            return HttpResponseForbidden()
        return HttpResponse("OK")
