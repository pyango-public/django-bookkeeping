import datetime

import factory.fuzzy
from django.utils.crypto import get_random_string

from project.base.apps.bookkeeping.models import Company

now = datetime.datetime.now()
bank_tags = ['bank transfer', 'bank']
credit_card_tags = ['credit card']


class InvoiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'bookkeeping.Invoice'

    identifier = factory.Sequence(lambda n: get_random_string(10).upper())
    company = factory.Iterator(Company.objects.all())
    organisation = factory.SelfAttribute('company.organisation')
    title = factory.LazyAttribute(lambda o: '%s Invoice' % (o.company.name,))
    invoice_date = factory.fuzzy.FuzzyDate(
        datetime.date(now.year - 1, 1, 1),
        datetime.date(now.year, now.month, now.day),
    )
    amount = factory.Faker('pyint', min_value=1000, max_value=10000)
    paid_date = factory.SelfAttribute('invoice_date')
    pdf = factory.django.FileField(
        from_path='./document.pdf',
        filename='invoice.pdf'
    )
    invoice_type = factory.Iterator(['cc', 'bank'])


class InvoiceBankTransferFactory(InvoiceFactory):
    invoice_type = 'bank'

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            # A list of tags were passed in, use them.
            for tag in extracted:
                self.tags.add(tag)
        else:
            self.tags.add(*bank_tags)


class InvoiceCreditCardFactory(InvoiceFactory):
    invoice_type = 'cc'

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            # A list of tags were passed in, use them.
            for tag in extracted:
                self.tags.add(tag)
        else:
            self.tags.add(*credit_card_tags)
