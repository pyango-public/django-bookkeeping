import factory

from project.base.apps.organisation.models import Organisation


class DocumentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'bookkeeping.Document'

    title = factory.Faker('company')
    pdf = factory.django.FileField(
        from_path='./document.pdf',
        filename='document.pdf'
    )
    organisation = factory.Iterator(Organisation.objects.all())
