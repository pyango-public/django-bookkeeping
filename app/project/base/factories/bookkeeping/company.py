import factory

from project.base.apps.organisation.models import Organisation


class CompanyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'bookkeeping.Company'

    name = factory.Faker('company')
    organisation = factory.Iterator(Organisation.objects.all())
