import factory
from django.contrib.auth import get_user_model

from project.base.apps.organisation.models import Organisation

User = get_user_model()


class SalaryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'bookkeeping.Salary'

    comment = factory.Faker('text')
    amount = factory.Faker('pyint', min_value=1000, max_value=10000)
    user = factory.Iterator(User.objects.all())
    organisation = factory.Iterator(Organisation.objects.all())
