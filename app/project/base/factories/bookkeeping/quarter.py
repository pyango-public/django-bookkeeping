import datetime

import factory

from project.base.apps.organisation.models import Organisation

now = datetime.datetime.now()


class QuarterFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'bookkeeping.Quarter'
        django_get_or_create = ('quarter', 'year', 'organisation')

    year = factory.Faker('pyint', min_value=now.year - 1, max_value=now.year)
    quarter = factory.Iterator([1, 2, 3, 4])
    organisation = factory.Iterator(Organisation.objects.all())
