import datetime

import factory.fuzzy

from project.base.apps.bookkeeping.models import Quarter, Salary, CustomerInvoice, Invoice

now = datetime.datetime.now()


class BaseTransactionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "bookkeeping.BankTransaction"

    date = factory.fuzzy.FuzzyDate(
        datetime.date(now.year - 1, 1, 1),
        datetime.date(now.year, now.month, now.day),
    )
    quarter = factory.Iterator(Quarter.objects.all())
    organisation = factory.SelfAttribute('quarter.organisation')


class TransactionFactory(BaseTransactionFactory):
    salary = factory.Iterator(Salary.objects.all())
    debit_amount = factory.SelfAttribute('salary.amount')
    title = factory.LazyAttribute(
        lambda o: 'Transfer Salary Employee %s %s' % (o.salary.user.first_name, o.salary.user.last_name))


class TransactionWithCustomerInvoiceFactory(BaseTransactionFactory):
    customer_invoice = factory.Iterator(CustomerInvoice.objects.all())
    credit_amount = factory.SelfAttribute('customer_invoice.amount')
    title = factory.LazyAttribute(
        lambda o: '%s Transaction' % (o.customer_invoice.title,))


class TransactionWithInvoiceFactory(BaseTransactionFactory):
    invoice = factory.Iterator(Invoice.objects.all())
    debit_amount = factory.SelfAttribute('invoice.amount')
    title = factory.LazyAttribute(
        lambda o: '%s Transaction' % (o.invoice.title,))
