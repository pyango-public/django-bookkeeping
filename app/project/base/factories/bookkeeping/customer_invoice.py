import datetime

import factory.fuzzy
from dateutil.relativedelta import relativedelta
from django.utils.crypto import get_random_string

from project.base.apps.bookkeeping.models import Company

now = datetime.datetime.now()


class CustomerInvoiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'bookkeeping.CustomerInvoice'

    identifier = factory.Sequence(lambda n: get_random_string(10).upper())
    company = factory.Iterator(Company.objects.all())
    organisation = factory.SelfAttribute('company.organisation')
    title = factory.LazyAttribute(lambda o: '%s Invoice' % (o.company.name,))
    invoice_date = factory.fuzzy.FuzzyDate(
        datetime.date(now.year - 1, 1, 1),
        datetime.date(now.year, now.month, now.day),
    )
    amount = factory.Faker('pyint', min_value=1000, max_value=10000)
    sent_date = factory.SelfAttribute('invoice_date')
    due_date = factory.LazyAttribute(lambda o: o.invoice_date + relativedelta(months=1))
