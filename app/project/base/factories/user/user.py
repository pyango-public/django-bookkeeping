import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "user.User"
        django_get_or_create = ('username',)

    is_active = True
    is_staff = True
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    username = factory.Faker('email')
