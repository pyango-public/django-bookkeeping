import factory

from project.base.factories.user import UserFactory


class EmailFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "user.Email"
        django_get_or_create = ('email',)

    validated = True
    email = factory.Faker('email')
    main_email = True
    user = factory.SubFactory(UserFactory, username=factory.SelfAttribute('..email'))
