import factory


class OrganisationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'organisation.Organisation'
        django_get_or_create = ('name',)

    name = factory.Faker('company')
    phone_number = factory.Faker('phone_number')
    email = factory.Faker('company_email')
    address = factory.Faker('address')
    default_currency = factory.Iterator(['CHF', 'USD', 'EUR', 'AED'])
