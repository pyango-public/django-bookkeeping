from operator import attrgetter
import random
from typing import List

from django.core.management import BaseCommand

from project.base.factories.bookkeeping import CompanyFactory
from project.base.factories.bookkeeping.customer_invoice import CustomerInvoiceFactory
from project.base.factories.bookkeeping.document import DocumentFactory
from project.base.factories.bookkeeping.invoice import InvoiceBankTransferFactory, InvoiceCreditCardFactory
from project.base.factories.bookkeeping.quarter import QuarterFactory
from project.base.factories.bookkeeping.salary import SalaryFactory
from project.base.factories.bookkeeping.transaction import TransactionFactory, TransactionWithCustomerInvoiceFactory, \
    TransactionWithInvoiceFactory
from project.base.factories.organisations import OrganisationFactory
from project.base.factories.user import EmailFactory


class Command(BaseCommand):
    help = "Generate Fake Data"

    def add_arguments(self, parser):
        parser.add_argument("-l", "--length", nargs="?", type=int, default=10)

    @staticmethod
    def generate_user_with_email(length: int) -> List:
        return EmailFactory.create_batch(length)

    @staticmethod
    def generate_organisation(length: int) -> List:
        return OrganisationFactory.create_batch(length)

    @staticmethod
    def generate_companies(length: int, *, organizations: List = None) -> List:
        if organizations:
            return [CompanyFactory(organisation=random.choice(organizations)) for _ in range(length)]
        return CompanyFactory.create_batch(length)

    @staticmethod
    def generate_salaries(length: int, *, users: List = None, organisations: List = None) -> List:
        if users and organisations:
            return [SalaryFactory(
                user=random.choice(users),
                organisation=random.choice(organisations)
            ) for _ in range(length)]
        return SalaryFactory.create_batch(length)

    @staticmethod
    def generate_quarters(length: int, *, organisations: List = None) -> List:
        if organisations:
            return [QuarterFactory(organisation=random.choice(organisations)) for _ in range(length)]
        return QuarterFactory.create_batch(length)

    @staticmethod
    def generate_transactions(length: int, *, quarters: List = None, salaries: List = None) -> List:
        if quarters and salaries:
            return [TransactionFactory(
                quarter=random.choice(quarters),
                salary=random.choice(salaries)
            ) for _ in range(length)]
        return TransactionFactory.create_batch(length)

    @staticmethod
    def generate_documents(length: int, *, organisations: List = None) -> List:
        if organisations:
            return [DocumentFactory(organisation=random.choice(organisations)) for _ in range(length)]
        return DocumentFactory.create_batch(length)

    @staticmethod
    def generate_customer_invoices(length: int, *, companies: List = None) -> List:
        if companies:
            return [CustomerInvoiceFactory(company=random.choice(companies)) for _ in range(length)]
        return CustomerInvoiceFactory.create_batch(length)

    @staticmethod
    def generate_bank_transfer_invoices(length: int, *, companies: List = None) -> List:
        if companies:
            return [InvoiceBankTransferFactory(company=random.choice(companies)) for _ in range(length)]
        return InvoiceBankTransferFactory.create_batch(length)

    @staticmethod
    def generate_credit_card_invoices(length: int, *, companies: List = None) -> List:
        if companies:
            return [InvoiceCreditCardFactory(company=random.choice(companies)) for _ in range(length)]
        return InvoiceCreditCardFactory.create_batch(length)

    @staticmethod
    def generate_customer_invoice_transactions(length: int, *, quarters: List = None,
                                               customer_invoices: List = None) -> List:
        if quarters and customer_invoices:
            return [TransactionWithCustomerInvoiceFactory(
                quarter=random.choice(quarters),
                customer_invoice=random.choice(customer_invoices)
            ) for _ in range(length)]
        return TransactionWithCustomerInvoiceFactory().create_batch(length)

    @staticmethod
    def generate_invoice_transactions(length: int, *, quarters: List = None, invoices: List = None) -> List:
        if quarters and invoices:
            return [TransactionWithInvoiceFactory(
                quarter=random.choice(quarters),
                invoice=random.choice(invoices)
            ) for _ in range(length)]
        return TransactionWithInvoiceFactory.create_batch(length)

    def save_fake_data_ids(self, model: str, ids: List):
        ids = ','.join(list(map(str, ids)))
        with open('./fake_data.txt', 'a+') as op:
            op.write(f'{model}:{ids}\n')
            self.stdout.write(f'{model} were saved')

    def handle(self, *args, **options):
        length = options.get("length")
        self.stdout.write(f'Starting to generate fake data, length: {length}')

        self.stdout.write('Generation fake date for the Email and user')
        emails = self.generate_user_with_email(length)
        self.stdout.write('Fake data for the Email and user were generated')

        emails_ids = list(map(attrgetter('id'), emails))
        self.save_fake_data_ids('user__Email', emails_ids)
        users_ids = list(map(attrgetter('user_id'), emails))
        self.save_fake_data_ids('user__User', users_ids)

        self.stdout.write('Generation fake date for the Organization')
        organisations = self.generate_organisation(int(length / 2))
        self.stdout.write('Fake data for the Organisation were generated')

        organisations_ids = list(map(attrgetter('id'), organisations))
        self.save_fake_data_ids('organisation__Organisation', organisations_ids)

        self.stdout.write('Generation fake date for the Company')
        companies = self.generate_companies(int(length / 2), organizations=organisations)
        self.stdout.write('Fake data for the Company were generated')

        companies_ids = list(map(attrgetter('id'), companies))
        self.save_fake_data_ids('bookkeeping__Company', companies_ids)

        for i, company in enumerate(companies):
            company.organisation = organisations[i]
            company.save()

        organisations_iter = iter(organisations)
        for email in emails:
            try:
                organisation = next(organisations_iter)
            except StopIteration:
                organisations_iter = iter(organisations)
                organisation = next(organisations_iter)
            organisation.members.add(email.user)

        self.stdout.write('Generation fake date for the Salary')
        salaries = self.generate_salaries(
            length, users=list(map(attrgetter('user'), emails)), organisations=organisations
        )
        self.stdout.write('Fake data for the Salary were generated')
        salaries_ids = list(map(attrgetter('id'), salaries))
        self.save_fake_data_ids('bookkeeping__Salary', salaries_ids)

        self.stdout.write('Generation fake date for the Quarter')
        quarters = self.generate_quarters(length, organisations=organisations)
        self.stdout.write('Fake data for the Quarter were generated')
        quarters_ids = list(map(attrgetter('id'), quarters))
        self.save_fake_data_ids('bookkeeping__Quarter', quarters_ids)

        self.stdout.write('Generation fake date for the Transaction')
        transactions = self.generate_transactions(length, quarters=quarters, salaries=salaries)
        self.stdout.write('Fake data for the transaction were generated')
        transactions_ids = list(map(attrgetter('id'), transactions))
        self.save_fake_data_ids('bookkeeping__BankTransaction', transactions_ids)

        self.stdout.write('Generation fake date for the Document')
        documents = self.generate_documents(int(length / 2), organisations=organisations)
        self.stdout.write('Fake data for the Document were generated')
        documents_ids = list(map(attrgetter('id'), documents))
        self.save_fake_data_ids('bookkeeping__Document', documents_ids)

        self.stdout.write('Generation fake date for the Customer Invoice')
        customer_invoices = self.generate_customer_invoices(length, companies=companies)
        self.stdout.write('Fake data for the Customer Invoice were generated')
        customer_invoices_ids = list(map(attrgetter('id'), customer_invoices))
        self.save_fake_data_ids('bookkeeping__CustomerInvoice', customer_invoices_ids)

        self.stdout.write('Generation fake date for the Invoice with BankTransfer')
        bank_transfer_invoices = self.generate_bank_transfer_invoices(int(length / 2), companies=companies)
        self.stdout.write('Fake data for the Invoice with BankTransfer were generated')
        bank_transfer_invoices_ids = list(map(attrgetter('id'), bank_transfer_invoices))
        self.save_fake_data_ids('bookkeeping__Invoice', bank_transfer_invoices_ids)

        self.stdout.write('Generation fake date for the Invoice with Credit Card')
        credit_card_invoices = self.generate_credit_card_invoices(int(length / 2), companies=companies)
        self.stdout.write('Fake data for the Invoice with Credit Card were generated')
        credit_card_invoices_ids = list(map(attrgetter('id'), credit_card_invoices))
        self.save_fake_data_ids('bookkeeping__Invoice', credit_card_invoices_ids)

        self.stdout.write('Generation fake date for the Transaction with Invoice')
        invoice_transactions = self.generate_invoice_transactions(
            int(length / 2), quarters=quarters, invoices=credit_card_invoices + bank_transfer_invoices
        )
        self.stdout.write('Fake data for the Transaction with Invoice were generated')
        invoice_transactions_ids = list(map(attrgetter('id'), invoice_transactions))
        self.save_fake_data_ids('bookkeeping__BankTransaction', invoice_transactions_ids)

        self.stdout.write('Generation fake date for the Transaction with Customer Invoice')
        customer_invoice_transactions = self.generate_customer_invoice_transactions(
            int(length / 2), quarters=quarters, customer_invoices=customer_invoices)
        self.stdout.write('Fake data for the Transaction with Customer Invoice were generated')
        customer_invoice_transactions_ids = list(map(attrgetter('id'), customer_invoice_transactions))
        self.save_fake_data_ids('bookkeeping__BankTransaction', customer_invoice_transactions_ids)
