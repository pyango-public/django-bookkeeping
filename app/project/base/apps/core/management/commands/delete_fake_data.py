import os

from django.core.management import BaseCommand
from factory.django import get_model


class Command(BaseCommand):
    help = "Delete Fake Data"
    fake_data_file = './fake_data.txt'

    def handle(self, *args, **options):
        if not os.path.exists(self.fake_data_file):
            self.stderr.write('Fake date does not exist')
            return
        self.stdout.write('Starting to delete fake data')
        with open(self.fake_data_file) as f:
            for line in f.readlines():
                app_model, ids = line.split(':')
                app, model = app_model.split('__')
                model = get_model(app, model)
                model.objects.filter(id__in=ids.split(',')).delete()
                self.stdout.write('Fake data deleted for model %s: %s' % (model.__name__, ids))
        os.remove('./fake_data.txt')
