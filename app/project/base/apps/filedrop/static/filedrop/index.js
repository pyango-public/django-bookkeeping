function copyText(textToCopy) {
  const myTemporaryInputElement = document.createElement('input')
  myTemporaryInputElement.type = 'text'
  myTemporaryInputElement.value = textToCopy
  document.body.appendChild(myTemporaryInputElement)
  myTemporaryInputElement.select()
  document.execCommand('Copy')
  document.body.removeChild(myTemporaryInputElement)
}
document.addEventListener('DOMContentLoaded', function (event) {
  document.getElementById("searchbar").placeholder = "Search here..";
  document.querySelectorAll('.copy_public_link').forEach(item => {
    item.addEventListener('click', async event => {
      event.preventDefault()
      event.stopPropagation()
      if (!item.active) {
        item.active = true
        item.textContent = "Loading...";
        const result = await fetch(item.dataset.url).then(resp => resp.json())
        copyText(result.url)
        item.textContent = "Link copied";
        setTimeout(() => {
          item.textContent = "Copy public link";
          item.active = false
        }, 1000)
      }
    })
  })
})