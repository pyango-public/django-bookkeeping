from urllib.parse import urlencode

import boto3
import requests
from django.conf import settings
from django.contrib import admin
from django.http import JsonResponse
from django.urls import path, reverse
from django.utils.safestring import mark_safe
from rest_framework.exceptions import NotFound

from .models import Upload


@admin.register(Upload)
class UploadAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'is_private', 'user', 'copy_public_link_1', 'copy_public_link_7', 'copy_permanent_public_link'
    ]
    search_fields = ['name', 'file']
    list_display_links = ['name']
    actions = ['mark_as_public', 'mark_as_private']
    readonly_fields = ['user']

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            return queryset.filter(user=request.user)
        return queryset

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)

    def copy_public_link_7(self, obj):
        if obj.is_private:
            qp = {
                'expire': 7 * 24 * 3600  # valid for one month
            }
            return mark_safe(f'<button class="copy_public_link" '
                             f'data-url={reverse("admin:get_public_link", kwargs={"pk": obj.pk})}?{urlencode(qp)}>'
                             f'Copy public link'
                             f'</button>')
        return None

    copy_public_link_7.allow_tags = True
    copy_public_link_7.short_description = 'Valid 7 days'

    def copy_public_link_1(self, obj):
        if obj.is_private:
            qp = {
                'expire': 24 * 3600  # valid for one day
            }
            return mark_safe(f'<button class="copy_public_link" '
                             f'data-url={reverse("admin:get_public_link", kwargs={"pk": obj.pk})}?{urlencode(qp)}>'
                             f'Copy public link'
                             f'</button>')
        return None

    copy_public_link_1.allow_tags = True
    copy_public_link_1.short_description = 'Valid 1 day'

    def copy_permanent_public_link(self, obj):
        qp = {
            'expire': 0,  # valid forever
            'permanent_link': obj.file
        }
        if not obj.is_private:
            return mark_safe(f'<button class="copy_public_link" '
                             f'data-url={reverse("admin:get_public_link", kwargs={"pk": obj.pk})}?{urlencode(qp)}>'
                             f'Copy public link'
                             f'</button>')
        return None

    copy_permanent_public_link.allow_tags = True
    copy_permanent_public_link.short_description = 'Valid forever'

    def get_public_link(self, request, *args, **kwargs):
        expire = int(request.GET.get('expire'))
        permanent_link = request.GET.get('permanent_link')
        if expire == 0 and permanent_link:
            return JsonResponse({'url': permanent_link})
        try:
            upload = Upload.objects.get(id=kwargs.get('pk'))
        except Upload.DoesNotExist:
            raise NotFound('Upload was not found!')
        session = boto3.session.Session()
        client = session.client(
            service_name='s3',
            region_name=settings.AWS_S3_REGION_NAME,
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        )
        presigned_url = client.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket': settings.AWS_STORAGE_BUCKET_NAME,
                'Key': upload.file.replace(f'{settings.AWS_S3_ENDPOINT_URL}/{settings.AWS_STORAGE_BUCKET_NAME}/',
                                           ''),
            },
            ExpiresIn=expire,
        )
        return JsonResponse({'url': presigned_url})

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('get-public-link/<int:pk>/', self.admin_site.admin_view(self.get_public_link),
                 name='get_public_link'),
        ]
        return my_urls + urls

    def change_acl(self, queryset, acl):
        s3 = boto3.resource(
            service_name='s3',
            region_name=settings.AWS_S3_REGION_NAME,
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        )
        for item in queryset:
            key = f'''{item.file.replace(settings.AWS_S3_ENDPOINT_URL + "/" +
                                                settings.AWS_STORAGE_BUCKET_NAME + "/", "")}'''
            object_acl = s3.ObjectAcl(settings.AWS_STORAGE_BUCKET_NAME, key)
            object_acl.put(ACL=acl)
            r = requests.head(item.file)
            if r.status_code == 200:
                item.is_private = False
                item.save()
            elif r.status_code == 403:
                item.is_private = True
                item.save()

    def mark_as_public(self, request, queryset):
        self.change_acl(queryset, acl='public-read')

    mark_as_public.short_description = "Mark as public"

    def mark_as_private(self, request, queryset):
        self.change_acl(queryset, acl='private')

    mark_as_private.short_description = "Mark as private"

    class Media:
        js = ['filedrop/index.js']
        css = {
            'all': ['filedrop/index.css'],
        }
