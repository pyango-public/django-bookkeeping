from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from s3direct.fields import S3DirectField
from taggit.managers import TaggableManager


class Upload(TimeStampedModel):
    name = models.CharField(
        verbose_name=_('file'),
        max_length=200,
        blank=True,
        null=True,
    )
    file = S3DirectField(
        verbose_name=_('file'),
        dest='default'
    )
    is_private = models.BooleanField(
        verbose_name="Is private",
        default=True,
        editable=False
    )
    user = models.ForeignKey(
        verbose_name=_('user'),
        to='user.User',
        related_name='filedrops',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    organisation = models.ForeignKey(
        verbose_name=_('organisation'),
        to='organisation.Organisation',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    tags = TaggableManager(
        verbose_name=_('tags'),
        blank=True,
    )
    folder = models.CharField(
        verbose_name=_('folder'),
        max_length=255,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name or str(self.file)

    class Meta:
        ordering = ['-created']
        verbose_name = _('Upload')
        verbose_name_plural = _('Uploads')
