from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from project import settings


class Organisation(TimeStampedModel):
    name = models.CharField(
        verbose_name=_('name'),
        max_length=255,
        unique=True,
    )
    members = models.ManyToManyField(
        verbose_name=_('members'),
        to='user.User',
        related_name='organisations',
        blank=True,
    )
    email = models.EmailField(
        verbose_name=_('email'),
        null=True,
        blank=True,
    )
    phone_number = models.CharField(
        verbose_name=_('phone number'),
        max_length=128,
        null=True,
        blank=True,
    )
    address = models.TextField(
        verbose_name=_('address'),
        max_length=1000,
        null=True,
        blank=True,
    )
    default_currency = models.CharField(
        verbose_name=_('default currency'),
        max_length=5,
        choices=settings.CURRENCIES,
        default='CHF',
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Organisation')
        verbose_name_plural = _('Organisations')
        ordering = ['name']
