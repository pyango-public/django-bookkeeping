from datetime import datetime

from admin_totals.admin import ModelAdminTotals
from dateutil import parser
from django.contrib import admin
from django.db import models
from django.db.models import FloatField, Q, Sum
from django.db.models.functions import Coalesce
from django.forms import NumberInput, TextInput
from django.utils.safestring import mark_safe

from .models import CustomerInvoice, Document, Invoice, Quarter, Salary, BankTransaction, Company


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    search_fields = [
        'name',
    ]
    list_filter = [
        'created', 'organisation'
    ]
    list_display = [
        'name', 'organisation'
    ]


class BankTransactionInline(admin.StackedInline):
    model = BankTransaction
    extra = 0
    autocomplete_fields = [
        'customer_invoice',
        'invoice',
        'salary',
    ]
    formfield_overrides = {
        models.DecimalField: {'widget': NumberInput(attrs={'size': '8'})},
    }
    readonly_fields = ['title', 'date', 'debit_amount', 'credit_amount']
    fields = ['title', 'date', 'debit_amount', 'credit_amount', 'customer_invoice', 'invoice', 'salary']


class InvoiceInline(admin.StackedInline):
    model = Invoice
    extra = 0


@admin.register(CustomerInvoice)
class CustomerInvoiceAdmin(ModelAdminTotals):
    list_display = [
        'title', 'identifier', 'company', 'pdf_link', 'amount',
        'invoice_date', 'sent_date', 'due_date', 'paid_date',
        'status',
    ]
    list_display_links = [
        'identifier', 'title',
    ]
    search_fields = [
        'identifier',
        'title',
        'company__name',
        'amount',
    ]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'style': 'width:80%;'})},
    }
    autocomplete_fields = ['company']
    list_totals = [('amount', lambda field: Coalesce(Sum(field), 0.00, output_field=FloatField()))]
    save_as = True
    save_on_top = True

    def status(self, obj):
        if obj.paid_date:
            return mark_safe('<span style="color:green;">paid</span>')
        if not obj.due_date:
            return mark_safe('<span style="color:gray;">draft</span>')
        if obj.due_date < datetime.today().date():
            delta = datetime.today().date() - obj.due_date
            return mark_safe(f'<span style="color:red;">overdue {delta.days} days</span>')
        return '-'

    def pdf_link(self, obj):
        if obj.pdf:
            return mark_safe(f'<a href="{obj.pdf.url}" target="_blank">View</a>')
        else:
            return "No pdf"

    pdf_link.short_description = 'PDF'


@admin.register(Invoice)
class InvoiceAdmin(ModelAdminTotals):
    list_display = [
        'title', 'company', 'invoice_type', 'get_amount',
        'contains_vat', 'for_private_use', 'pdf_link', 'invoice_date', 'paid_date', 'invoice',
        'tag_list',
    ]
    list_display_links = [
        'title',
    ]
    search_fields = [
        'identifier',
        'title',
        'amount',
        'invoice_type',
        'company__name',
        'invoice__title',
        'invoice_date',
        'paid_date',
    ]
    list_filter = [
        'currency',
        'for_private_use',
        'contains_vat',
        'tags',
    ]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'style': 'width:80%;'})},
    }
    autocomplete_fields = [
        'company',
        'invoice',
        'paid_by',
    ]
    inlines = [InvoiceInline]
    list_editable = [
        'invoice',
    ]
    save_as = True
    save_on_top = True

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())

    def get_amount(self, obj):
        return f'{obj.amount} {obj.currency}'

    def pdf_link(self, obj):
        if obj.pdf:
            return mark_safe(f'<a href="{obj.pdf.url}" target="_blank">View</a>')
        else:
            return "No pdf"

    pdf_link.short_description = 'PDF'

    def paid_by_user(self, obj):
        if obj.paid_by:
            return obj.paid_by.full_name or obj.paid_by
        return obj.paid_by

    paid_by_user.short_description = 'Paid by'

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        try:
            if 'organisation' in form.base_fields and form.base_fields['organisation'].initial:
                last_invoice = Invoice.objects.filter(
                    organisation=form.base_fields['organisation'].initial
                ).latest('created')
                form.base_fields['invoice_type'].initial = last_invoice.invoice_type
                form.base_fields['company'].initial = last_invoice.company
                form.base_fields['invoice_date'].initial = last_invoice.invoice_date
        except Invoice.DoesNotExist:
            pass
        return form

    def get_search_results(self, request, queryset, search_term):
        queryset, may_have_duplicates = super().get_search_results(
            request, queryset, search_term,
        )
        try:
            date = parser.parse(search_term)
        except ValueError:
            pass
        else:
            queryset |= self.model.objects.filter(
                Q(invoice_date=date) | Q(paid_date=date)
            )
        return queryset, may_have_duplicates

    get_amount.short_description = 'amount'


@admin.register(Quarter)
class QuarterAdmin(admin.ModelAdmin):
    list_display = [
        '__str__', 'pdf_link', 'start_date', 'end_date',
        'total_revenue', 'total_expenses', 'total_salaries', 'total_profit',
        'organisation',
    ]
    list_filter = [
        'quarter',
        'year',
    ]
    save_on_top = True
    inlines = [BankTransactionInline]
    search_fields = ['quarter', 'year']

    def pdf_link(self, obj):
        if obj.pdf:
            return mark_safe(f'<a href="{obj.pdf.url}" target="_blank">View</a>')
        else:
            return "No pdf"

    pdf_link.short_description = 'PDF'

    def total_revenue(self, obj):
        return f'{obj.total_revenue} CHF'

    total_revenue.short_description = 'Total revenue'

    def total_expenses(self, obj):
        return f'{obj.total_expenses} CHF'

    total_expenses.short_description = 'Total expenses'

    def total_salaries(self, obj):
        return f'{obj.total_salaries} CHF'

    total_salaries.short_description = 'Total salaries'

    def total_profit(self, obj):
        return f'{obj.total_profit} CHF'

    total_profit.short_description = 'Total profit'


@admin.register(Salary)
class SalaryAdmin(ModelAdminTotals):
    list_display = ['username', 'amount', 'paid_date', 'compensation', 'report_link', 'comment']
    list_filter = [
        'compensation', 'organisation'
    ]
    search_fields = [
        'user__username',
        'user__first_name',
        'user__last_name',
        'amount',
        'comment',
    ]
    autocomplete_fields = [
        'user',
    ]
    list_totals = [('amount', lambda field: Coalesce(Sum(field), 0.00, output_field=FloatField()))]
    save_as = True
    save_on_top = True

    def username(self, obj):
        if obj.user:
            return obj.user.full_name or obj.user
        return obj.user

    def report_link(self, obj):
        if obj.report:
            return mark_safe(f'<a href="{obj.report.url}" target="_blank">View</a>')
        else:
            return "No report"

    username.short_description = 'User'
    report_link.short_description = 'Report'


@admin.register(BankTransaction)
class BankTransactionAdmin(ModelAdminTotals):
    search_fields = [
        'ref_number',
        'title',
        'debit_amount',
        'credit_amount',
        'comment',
    ]
    list_display = [
        'title',
        'date',
        'debit_amount',
        'credit_amount',
        'invoice',
        'customer_invoice',
        'salary',
        'comment',
    ]
    list_editable = [
        'invoice',
        'customer_invoice',
        'salary',
        'comment',
    ]
    autocomplete_fields = [
        'quarter',
        'customer_invoice',
        'invoice',
        'salary',
    ]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'style': 'width:80%;'})},
    }
    readonly_fields = ['ref_number']
    list_totals = [
        ('debit_amount', lambda field: Coalesce(Sum(field), 0.00, output_field=FloatField())),
        ('credit_amount', lambda field: Coalesce(Sum(field), 0.00, output_field=FloatField())),
    ]
    actions = ['assign_organisation']

    class Media:
        css = {
            'all': ['column_width.css']
        }


@admin.register(Document)
class DocumentAdmin(ModelAdminTotals):
    list_display = ['title', 'pdf_link', 'created']
    search_fields = ['title']
    list_filter = ['created', 'organisation']

    def pdf_link(self, obj):
        if obj.pdf:
            return mark_safe(f'<a href="{obj.pdf.url}" target="_blank">View</a>')
        else:
            return "No pdf"

    pdf_link.short_description = 'PDF'
