from datetime import datetime, timedelta

from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from project.base.apps.bookkeeping.models.salary import Salary


class Quarter(TimeStampedModel):
    quarter = models.IntegerField(
        verbose_name=_('quarter'),
        choices=[
            (1, 'Q1'),
            (2, 'Q2'),
            (3, 'Q3'),
            (4, 'Q4'),
        ]
    )
    year = models.IntegerField(
        verbose_name=_('year'),
    )
    pdf = models.FileField(
        verbose_name=_('PDF'),
        blank=True,
        null=True,
    )
    csv = models.FileField(
        verbose_name=_('csv'),
        help_text=_('csv transactions export from the bank website, used for import transactions in helios'),
        blank=True,
        null=True,
    )
    organisation = models.ForeignKey(
        verbose_name=_('organisation'),
        to='organisation.Organisation',
        on_delete=models.SET_NULL,
        null=True,
    )

    @property
    def start_date(self):
        return datetime(self.year, 3 * self.quarter - 2, 1).date()

    @property
    def end_date(self):
        year = self.year
        month = 3 * self.quarter + 1
        if month > 12:
            month = 1
            year += 1
        return (datetime(year, month, 1) + timedelta(days=-1)).date()

    @property
    def total_revenue(self):
        return sum(t.credit_amount or 0 for t in self.bank_transactions.all())

    @property
    def total_expenses(self):
        return sum(t.debit_amount or 0 for t in self.bank_transactions.all())

    @property
    def total_salaries(self):
        return sum(s.amount or 0 for s in Salary.objects.filter(
            organisation=self.organisation,
            paid_date__gte=self.start_date,
            paid_date__lte=self.end_date
        ))

    @property
    def total_profit(self):
        return self.total_revenue - self.total_expenses

    def __str__(self):
        return f'Q{self.quarter} - {self.year}'

    class Meta:
        verbose_name = _('Quarter')
        verbose_name_plural = _('Quarters')
        unique_together = [
            ('quarter', 'year', 'organisation'),
        ]
        ordering = ['organisation', '-year', '-quarter']
