from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from taggit.managers import TaggableManager


class Invoice(TimeStampedModel):
    INVOICE_TYPES = [
        ('cc', 'Credit Card'),
        ('bank', 'Bank Transfer'),
    ]
    invoice_type = models.CharField(
        verbose_name=_('invoice type'),
        choices=INVOICE_TYPES,
        max_length=10,
    )
    identifier = models.CharField(
        verbose_name=_('identifier'),
        max_length=20,
        blank=True,
        null=True,
    )
    title = models.CharField(
        verbose_name=_('title'),
        max_length=255,
    )
    company = models.ForeignKey(
        verbose_name=_('company'),
        to='bookkeeping.Company',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    pdf = models.FileField(
        verbose_name=_('pdf'),
    )
    amount = models.DecimalField(
        verbose_name=_('amount'),
        max_digits=10,
        decimal_places=2,
    )
    currency = models.CharField(
        verbose_name=_('currency'),
        max_length=5,
        choices=settings.CURRENCIES,
        default='CHF',
    )
    invoice_date = models.DateField(
        verbose_name=_('invoice date'),
    )
    paid_date = models.DateField(
        verbose_name=_('paid date'),
        null=True,
        blank=True,
    )
    paid_by = models.ForeignKey(
        verbose_name=_("paid by"),
        to="user.User",
        related_name='paid_invoices',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    comment = models.TextField(
        verbose_name=_('comment'),
        blank=True,
        null=True,
    )
    invoice = models.ForeignKey(
        verbose_name=_("invoice"),
        help_text=_('used to link invoices to each other'),
        to="bookkeeping.Invoice",
        related_name='invoices',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    organisation = models.ForeignKey(
        verbose_name=_('organisation'),
        to='organisation.Organisation',
        on_delete=models.SET_NULL,
        null=True,
    )
    for_private_use = models.BooleanField(
        verbose_name=_('Private'),
        help_text=_('This payment is for private spending\'s and should be deducted from the salary'),
        default=False,
    )
    contains_vat = models.BooleanField(
        verbose_name=_('VAT'),
        help_text=_('This payment contains VAT'),
        default=False,
    )
    tags = TaggableManager(
        verbose_name=_('tags'),
        help_text=_('Tag invoices for easy filtering'),
        blank=True,
    )

    def __str__(self):
        invoice_date = ''
        paid_date = ''
        if self.invoice_date:
            invoice_date = self.invoice_date.strftime("%d.%m.%y")
        if self.paid_date:
            paid_date = self.paid_date.strftime("%d.%m.%y")
        return f'{self.amount} {self.currency} {self.title} ' \
               f'{invoice_date}, Paid: {paid_date}, {self.company}'

    class Meta:
        ordering = ['-invoice_date']
        verbose_name = _('Invoice')
        verbose_name_plural = _('Invoices')
