from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class BankTransaction(TimeStampedModel):
    ref_number = models.CharField(
        verbose_name=_('ref number'),
        max_length=255,
        blank=True,
        null=True,
    )
    date = models.DateField(
        verbose_name=_('date'),
        null=True,
    )
    title = models.CharField(
        verbose_name=_('title'),
        max_length=255,
        blank=True,
        null=True,
    )
    debit_amount = models.DecimalField(
        verbose_name=_('debit amount'),
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
    )
    credit_amount = models.DecimalField(
        verbose_name=_('credit amount'),
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
    )
    # Fields for mapping
    quarter = models.ForeignKey(
        verbose_name=_('quarter'),
        to='bookkeeping.Quarter',
        on_delete=models.SET_NULL,
        related_name='bank_transactions',
        null=True,
        blank=True,
    )
    customer_invoice = models.ForeignKey(
        verbose_name=_("customer invoice"),
        to="bookkeeping.CustomerInvoice",
        related_name='bank_transactions',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    invoice = models.ForeignKey(
        verbose_name=_("invoice"),
        to="bookkeeping.Invoice",
        related_name='bank_transactions',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    salary = models.ForeignKey(
        verbose_name=_("salary"),
        to="bookkeeping.Salary",
        related_name='bank_transactions',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    organisation = models.ForeignKey(
        verbose_name=_('organisation'),
        to='organisation.Organisation',
        on_delete=models.SET_NULL,
        null=True,
    )
    comment = models.CharField(
        verbose_name=_('comment'),
        help_text=_('further information about the transaction'),
        max_length=255,
        null=True,
        blank=True,
    )

    def get_debit_or_credit_label(self):
        return f'+{self.credit_amount}' if self.credit_amount else f'-{self.debit_amount}'

    def __str__(self):
        return f'{self.get_debit_or_credit_label()} | {self.title} - {self.date}'

    class Meta:
        verbose_name = _('Bank Transaction')
        verbose_name_plural = _('Bank Transactions')
        ordering = ['-date']
