from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class Salary(TimeStampedModel):
    amount = models.DecimalField(
        verbose_name=_('amount'),
        max_digits=10,
        decimal_places=2,
    )
    user = models.ForeignKey(
        verbose_name=_("paid to user"),
        to="user.User",
        related_name='received_salaries',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    paid_date = models.DateField(
        verbose_name=_('paid date'),
        null=True,
        blank=True,
    )
    compensation = models.BooleanField(
        verbose_name=_('compensation'),
        default=False,
    )
    comment = models.TextField(
        verbose_name=_('comment'),
        help_text=_('further information about the salary'),
        null=True,
        blank=True,
    )
    organisation = models.ForeignKey(
        verbose_name=_('organisation'),
        to='organisation.Organisation',
        on_delete=models.SET_NULL,
        null=True,
    )
    report = models.FileField(
        verbose_name=_('report'),
        upload_to='bookkeeping/salary_reports',
        blank=True,
        null=True,
    )

    def __str__(self):
        result = f'{self.amount} - {self.paid_date}'
        if self.user:
            return f'{self.user.full_name or self.user} - {result}'
        return result

    class Meta:
        ordering = ['-paid_date']
        verbose_name = _('Salary')
        verbose_name_plural = _('Salaries')
