from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from taggit.managers import TaggableManager


class Document(TimeStampedModel):
    title = models.CharField(
        verbose_name=_('title'),
        max_length=255,
    )
    pdf = models.FileField(
        verbose_name=_('document'),
        upload_to='bookkeeping/document/',
        null=True,
    )
    organisation = models.ForeignKey(
        verbose_name=_('organisation'),
        to='organisation.Organisation',
        on_delete=models.SET_NULL,
        null=True,
    )
    tags = TaggableManager(
        verbose_name=_('tags'),
        blank=True,
    )

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created']
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')
