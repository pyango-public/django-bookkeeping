from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class CustomerInvoice(TimeStampedModel):
    identifier = models.CharField(
        verbose_name=_('identifier'),
        max_length=20,
        blank=True,
        null=True,
    )
    title = models.CharField(
        verbose_name=_('title'),
        max_length=255,
    )
    company = models.ForeignKey(
        verbose_name=_('company'),
        to='bookkeeping.Company',
        on_delete=models.SET_NULL,
        null=True,
    )
    pdf = models.FileField(
        verbose_name=_('pdf'),
        blank=True,
        null=True,
    )
    amount = models.DecimalField(
        verbose_name=_('amount'),
        max_digits=10,
        decimal_places=2,
    )
    invoice_date = models.DateField(
        verbose_name=_('invoice date'),
        blank=True,
        null=True,
    )
    sent_date = models.DateField(
        verbose_name=_('sent date'),
        help_text=_('the date when the invoice was sent to the client'),
        blank=True,
        null=True,
    )
    due_date = models.DateField(
        verbose_name=_('due date'),
        blank=True,
        null=True,
    )
    paid_date = models.DateField(
        verbose_name=_('paid date'),
        null=True,
        blank=True,
    )
    organisation = models.ForeignKey(
        verbose_name=_('organisation'),
        to='organisation.Organisation',
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return f'{self.amount} - {self.title}'

    class Meta:
        ordering = ['-invoice_date']
        verbose_name = _('Customer Invoice')
        verbose_name_plural = _('Customer Invoices')
