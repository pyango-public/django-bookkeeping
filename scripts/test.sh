#!/bin/bash
set -e

/usr/local/bin/coverage run ./manage.py test --noinput  # --parallel 6

# Grab the coverage for gitlab: use "TOTAL:\s+\d+\%" in the gitlab-ci-settings
LOG=$(mktemp)
/usr/local/bin/coverage report -m 2>&1 >$LOG
cat $LOG
grep TOTAL $LOG | awk '{ print "TOTAL: "$4; }'
rm $LOG

/usr/local/bin/coverage html
chmod 777 -R htmlcov/
