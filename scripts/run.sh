#!/bin/bash
/usr/local/bin/python manage.py migrate
/usr/local/bin/python manage.py collectstatic --noinput
exec /usr/local/bin/gunicorn -c /scripts/gunicorn.conf.py project.wsgi
