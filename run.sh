#!/usr/bin/env bash
sh stop.sh
docker-compose -p django-bookkeeping pull
docker-compose -p django-bookkeeping up -d
